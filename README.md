# README #

Please find attached my answers to the Skedulo questions. I have written these to be housed in their own little web-app created in React and Redux.

**Important:**
I am using the fetch web api for network transactions. To ensure you get the best results, please view this test on Chrome.

### Insructions ###
* Install dependencies.
Type `npm install` to install the dependencies. Given we're using react and redux boilerplate to create a webapp, this can take a minute.

* Start Node Server.
Question three needs the node server running to make API calls. Type `node server.js` to kick this off.

* Compile and run local dev server.
Type `npm run start` to compile the codebase and run it on http://localhost:8080/

* Run tests.
`npm run test` runs a few basic tests I have implemented.