export const SET_RICHEST = 'questionThree_SET_RICHEST';
export const SET_PEOPLE = 'questionThree_SET_PEOPLE';
export const SET_INTERESTS = 'questionThree_SET_INTERESTS';
export const SET_SKILLS = 'questionThree_SET_SKILLS';

export const SET_LOADING = 'questionFour_SET_LOADING';
export const SET_GITHUB_USER_DATA = 'questionFour_SET_GITHUB_USER_DATA';
