import { QuestionBonus } from '../QuestionBonus';
import React from 'react';
import { shallow } from 'enzyme';

import { bandData, currentTime } from '../../data/bonusData';

describe('genTotalSets', () => {
  it('generates correct amount of timeslots in the schedule', () => {
    const wrapper = shallow(
      <QuestionBonus
        bandData={bandData}
        currentTime={currentTime}
      />,
    );

    const result = wrapper.instance().genTotalSets();

    expect(JSON.stringify(result)).toBe('6');
  });
});

describe('genBaseSchedule', () => {
  it('can generate a base schedule to follow with all bands playing', () => {
    const wrapper = shallow(
      <QuestionBonus
        bandData={bandData}
        currentTime={currentTime}
      />,
    );

    const baseSchedule = wrapper.instance().genBaseSchedule(6);

    const expectedBaseSchedule = [
      [bandData[2]],
      [bandData[0], bandData[2]],
      [bandData[1], bandData[2]],
      [bandData[1]],
      [{ band: 'Wait' }],
      [bandData[3], bandData[4]]
    ];

    expect(baseSchedule).toEqual(expect.arrayContaining(expectedBaseSchedule));
  });
});

describe('genBandsPlayingNow', () => {
  it('returns band 3 for the first time slot', () => {
    const wrapper = shallow(
      <QuestionBonus
        bandData={bandData}
        currentTime={currentTime}
      />,
    );

    const bandsPlayingNow = wrapper.instance().genBandsPlayingNow(currentTime.getTime());
    const expectedResult =  [bandData[2]]

    expect(bandsPlayingNow).toEqual(expect.arrayContaining(expectedResult));
  });

  it('returns band 1 and 3 for the second time slot', () => {
    const wrapper = shallow(
      <QuestionBonus
        bandData={bandData}
        currentTime={currentTime}
      />,
    );

    const bandsPlayingNow = wrapper.instance().genBandsPlayingNow(currentTime.getTime() + 60000); // plus one minute
    const expectedResult = [bandData[0], bandData[2]];

    expect(bandsPlayingNow).toEqual(expect.arrayContaining(expectedResult));
  });
});

describe('genOptimisedSchedule', () => {
  it('return what bands we want to be seeing at any given time given band priority levels', () => {
    const wrapper = shallow(
      <QuestionBonus
        bandData={bandData}
        currentTime={currentTime}
      />,
    );

    const baseSchedule = [
      [bandData[2]],
      [bandData[0], bandData[2]],
      [bandData[1], bandData[2]],
      [bandData[1]],
      [{ band: 'Wait' }],
      [bandData[3], bandData[4]],
    ];

    const expectedOptimalResult = [
      {
        bandName: bandData[2].band,
        timePlaying: '1min',
      },
      {
        bandName: bandData[0].band,
        timePlaying: '2min',
      },
      {
        bandName: bandData[2].band,
        timePlaying: '3min',
      },
      {
        bandName: bandData[1].band,
        timePlaying: '4min',
      },
      {
        bandName: 'Wait',
        timePlaying: '5min',
      },
      {
        bandName: bandData[3].band,
        timePlaying: '6min',
      },
    ];

    const optimisedSchedule = wrapper.instance().genOptimisedSchedule(baseSchedule);

    expect(optimisedSchedule).toEqual(expect.arrayContaining(expectedOptimalResult));
  });
});
