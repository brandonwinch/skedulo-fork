import { QuestionThree } from '../QuestionThree';
import React from 'react';
import { shallow } from 'enzyme';

const peopleData = [
  {
    name: 'Bill Gates',
    id: '1',
    org: 'Microsoft',
  },
  {
    name: 'Steve Jobs',
    id: '2',
    org: 'Apple',
  },
];

const skillData = [
  {
    name: 'C++, Basic, Monopoly',
    personId: '1',
  },
  {
    name: 'Turtlenecks, Instagram',
    personId: '2',
  },
];

const interestData = [
  {
    name: 'Skiing, Philanthropy',
    personId: '1',
  },
  {
    name: 'Fonts',
    personId: '2',
  },
];

describe('findPersonInterests', () => {
  it('returns correct interests of person #1', () => {
    const wrapper = shallow(
      <QuestionThree
        fetchPeople={() => {}}
        fetchInterestsSkillsRichest={() => {}}
        interestData={interestData}
      />
    )

    const interests = wrapper.instance().findPersonInterests('1');
    const expected = 'Skiing, Philanthropy';

    expect(interests).toEqual(expected);
  });
});

describe('findPersonSkills', () => {
  it('returns correct skills of person #1', () => {
    const wrapper = shallow(
      <QuestionThree
        fetchPeople={() => {}}
        fetchInterestsSkillsRichest={() => {}}
        skillData={skillData}
      />,
    );

    const skills = wrapper.instance().findPersonSkills('1');
    const expected = 'C++, Basic, Monopoly';

    expect(skills).toEqual(expected);
  });
});

describe('generatePersonDetails', () => {
  it('returns person details of person #1 with bold class', () => {
    const wrapper = shallow(
      <QuestionThree
        fetchPeople={() => {}}
        fetchInterestsSkillsRichest={() => {}}
        interestData={interestData}
        skillData={skillData}
        richest={1}
      />,
    );

    const person = peopleData[0];
    const personDetails = wrapper.instance().generatePersonDetails(person);

    expect(personDetails).toEqual(expect.objectContaining({
      className: 'bold',
      personCompany: 'Microsoft',
      personName: 'Bill Gates',
      personInterests: 'Skiing, Philanthropy',
      personSkills: 'C++, Basic, Monopoly',
    }));
  });

  it('returns person details of person #2 without bold class', () => {
    const wrapper = shallow(
      <QuestionThree
        fetchPeople={() => {}}
        fetchInterestsSkillsRichest={() => {}}
        interestData={interestData}
        skillData={skillData}
      />,
    );

    const person = peopleData[1];
    const personDetails = wrapper.instance().generatePersonDetails(person);

    expect(personDetails).toEqual(expect.objectContaining({
      className: '',
      personCompany: 'Apple',
      personName: 'Steve Jobs',
      personInterests: 'Fonts',
      personSkills: 'Turtlenecks, Instagram',
    }));
  });
});
