import React from 'react';
import PropTypes, { object, number, func, shape, arrayOf } from 'prop-types';
import { connect } from 'react-redux';

import Table from '../components/Table';

class QuestionBonus extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      oneMinute: 60000,
      maxPriority: 1,
      minPriority: 10, // adjust if we want more than 10 levels of priority.
      totalSets: 0,
      optimisedSchedule: [],
    };
  }

  componentDidMount() {
    this.setOptimalSchedule();
  }

  /**
   *  With the data we bring in, we need to set an optimised schedule for
   *  Sally to use. We do this by first calculating the number of sets
   *  there will be in total, and then seeing which band plays in specific
   *  set times. Finally, we then pick which bands she should go to based
   *  on priority.
   */
  setOptimalSchedule() {
    // get number of sets
    const totalSets = this.genTotalSets();

    // generate a base schedule of who is playing when. this will be an array
    // filled with multiple objects.
    const schedule = this.genBaseSchedule(totalSets);

    const optimisedSchedule = this.genOptimisedSchedule(schedule);

    this.setState({
      optimisedSchedule,
      totalSets,
    });
  }

  /**
   *  Create a base schedule of which bands are playing when. We'll return
   *  an array populated with an object(s) that represent which bands are playing
   *  now.
   *
   *  @param {number} totalSets - How many sets we're playing for bands to fit in.
   *
   *  @return {array} baseSchedule - What bands are playing when.
   */
  genBaseSchedule(totalSets) {
    let currentSetTime = this.props.currentTime.getTime();
    const baseSchedule = [];

    // get a base schedule based off of total sets
    for (let i = 0; i < totalSets; i += 1) {
      const bandsPlayingNow = this.genBandsPlayingNow(currentSetTime);

      // for no bands that are playing now, insert a new 'dummy' band
      // item that we can reference later and just tell Sally to wait.
      if (bandsPlayingNow.length === 0) {
        bandsPlayingNow.push({ band: 'Wait' });
      }

      baseSchedule.push(bandsPlayingNow);
      currentSetTime += this.state.oneMinute;
    }

    return baseSchedule;
  }

  /**
   *  We want to find out what bands are playing in a given set time.
   *  We do this by comparing the band start and end times do the current
   *  set time.
   *
   *  @param {number} currentSetTime -  What the set time is that we're looking
   *                                    bands up against.
   *
   *  @return {array} - What bands are playing in this time slot.
   */
  genBandsPlayingNow(currentSetTime) {
    return this.props.bandData.filter((band) => {
      const bandStart = band.start.getTime();
      const bandFinish = band.finish.getTime();

      if (bandStart <= currentSetTime) {
        if (bandFinish > currentSetTime) {
          return true;
        }
      }
      return null;
    });
  }

  /**
   *  Generate the number of sets total that are being played
   *  at the concert. We do this by finding the band entry with
   *  the start time that is furtherest away from the current time.
   *
   *  @return {number} totalSets - How many sets is being played total.
   */
  genTotalSets() {
    let totalSets = 0;

    this.props.bandData.forEach((band) => {
      const difference = Math.abs(band.start.getTime() - this.props.currentTime.getTime());

      // since each set goes for one minute chunks.
      const setNumber = difference / this.state.oneMinute;

      if (totalSets < setNumber) {
        totalSets = setNumber;
      }
    });

    // since we start at 0, don't forget to increase at the end to get our last item.
    totalSets += 1;

    return totalSets;
  }

  /**
   *  With a schedule of who is playing when, we now need to calculate
   *  which band we will see based on their priority rating. We can do this
   *  by reducing down the function to a new one. We then map it to see only
   *  relevant data.
   *
   *  @param {array} schedule - Our basic schedule that tells who is playing when.
   *
   *  @return {array} An optimised schedule that only has who we want to see.
   */
  genOptimisedSchedule(schedule) {
    const opt = schedule.reduce((acc, val) => {
      // if there are concurrent bands going, choose the one with the higher priority
      if (val.length > 1) {
        let currentPriority = this.state.minPriority;
        let bestBandOnNow = null;

        val.forEach((band) => {
          // If a band has the same rating, we either can be selected.
          if (band.priority < currentPriority) {
            currentPriority = band.priority;
            bestBandOnNow = band;
          }
        });
        acc.push(bestBandOnNow);
      } else {
        acc.push(val[0]);
      }

      return acc;
    }, [])
    .map((band, idx) => {
      const timePlaying = `${idx + 1}min`;
      const bandName = band.band;

      return {
        timePlaying,
        bandName,
      };
    });

    return opt;
  }

  render() {
    return (
      <div>
        <p> Schedule:</p>
        {this.state.optimisedSchedule.length > 0 &&
          <Table
            data={this.state.optimisedSchedule}
          />
        }
      </div>
    );
  }
}

QuestionBonus.propTypes = {
  bandData: arrayOf(shape({
    band: PropTypes.string,
    start: object,
    finish: object,
    priority: number,
  })).isRequired,
  currentTime: shape({
    getTime: func,
  }).isRequired,
};

function mapStateToProps(state) {
  return {
    bandData: state.bandData,
    currentTime: state.currentTime,
  };
}

const QuestionBonusContainer = connect(mapStateToProps)(QuestionBonus);

export { QuestionBonus, QuestionBonusContainer };
