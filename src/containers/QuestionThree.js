import React from 'react';
import PropTypes, { shape, arrayOf, func } from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { fetchPeople, fetchInterestsSkillsRichest } from '../actions/questionThree';

import Table from '../components/Table';

class QuestionThree extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      groupPeople: [],
      tableHeaderData: ['Name', 'Company', 'Skills', 'Interests'],
    };
  }

  /**
   *  On component mount wewant to fetch a list of people. If successful, we then
   *  want to fetch interests, skills and who the richest person is. If this is all
   *  successful we then initialize further, mapping people to their relative interests,
   *  skills, etc.
   */
  componentDidMount() {
    this.props.fetchPeople()
    .then((ids) => {
      this.props.fetchInterestsSkillsRichest(ids)
      .then(() => {
        this.setGroupedPeople();
      }).catch((error) => {
        console.log('Something went wrong with the set group people promise:');
        console.log(error);
      });
    })
    .catch((error) => {
      // error.
      console.log('An error happened with the fetch skills promise. Make sure the node server is running.');
      console.log(error);
    });
  }

  /**
   *  Once we have fetched the people, skills and interests, we want to
   *  map all of their information together so it's easily accessible for
   *  us later on. We then update the state so we can pass it down as
   *  props.
   */
  setGroupedPeople() {
    const grouped = this.props.peopleData.map((person) => {
      const personDetails = this.generatePersonDetails(person);
      return personDetails;
    });

    this.setState({
      groupPeople: grouped,
    });
  }

  /**
   *  With the person object we find and map relative skills, interests, company and
   *  name and then return everything in a nice easy object.
   *
   *  @param {object} person - Basic details representing a person.
   *
   *  @return {object} - Mapped details related to each person.
   */
  generatePersonDetails(person) {
    let personSkills = this.findPersonSkills(person.id);
    let personInterests = this.findPersonInterests(person.id);

    if (personSkills.length === 0) {
      personSkills = 'None';
    }

    if (personInterests.length === 0) {
      personInterests = 'None';
    }

    const className = parseInt(person.id, 10) === this.props.richest ? 'bold' : '';

    return {
      personName: person.name,
      personCompany: person.org,
      personSkills,
      personInterests,
      className,
    };
  }

  /**
   *  By matching person IDs to our response return all of the skills this person has.
   *
   *  @param {string} id - Current ID of the person we want to look up.
   *
   *  @return {string} - All of the skills this person has, comma seperated.
   */
  findPersonSkills(id) {
    return this.props.skillData.filter(skill => (
      skill.personId === id
    ))
    .map(skill => (
      skill.name
    ))
    .join(', ');
  }

  /**
   *  By matching person IDs to our response we return all of the interests this person has.
   *
   *  @param {string} id - Current ID of the person we want to look up.
   *
   *  @return {string} - All of the interests this person has, comma seperated.
   */
  findPersonInterests(id) {
    return this.props.interestData.filter(interest => (
      interest.personId === id
    ))
    .map(interest => (
      interest.name
    ))
    .join(', ');
  }

  render() {
    if (this.state.groupPeople.length > 0) {
      return (
        <Table
          data={this.state.groupPeople}
          headerData={this.state.tableHeaderData}
        />
      );
    }
    return (
      <div />
    );
  }
}

QuestionThree.propTypes = {
  fetchPeople: func.isRequired,
  fetchInterestsSkillsRichest: func.isRequired,
  peopleData: arrayOf(shape({
    id: PropTypes.string,
    name: PropTypes.string,
    org: PropTypes.string,
  })),
  skillData: arrayOf(shape({
    name: PropTypes.string,
    personId: PropTypes.string,
  })),
  interestData: arrayOf(shape({
    name: PropTypes.string,
    personId: PropTypes.string,
  })),
  richest: PropTypes.number,
};

QuestionThree.defaultProps = {
  peopleData: [],
  skillData: [],
  interestData: [],
  richest: 0,
};

function mapStateToProps(state) {
  return {
    peopleData: state.peopleData,
    interestData: state.interestData,
    skillData: state.skillData,
    richest: state.richest,
  };
}

function mapDispatchToProps(dispatch) {
  const actionCreators = {
    fetchPeople,
    fetchInterestsSkillsRichest,
  };

  return bindActionCreators(actionCreators, dispatch);
}

const QuestionThreeContainer = connect(mapStateToProps, mapDispatchToProps)(QuestionThree);

export { QuestionThree, QuestionThreeContainer };
