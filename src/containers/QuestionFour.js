import React from 'react';
import PropTypes, { func, bool, shape, number, arrayOf } from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Table from '../components/Table';
import Loading from '../components/Loading';

import { searchGithubUsers, setGithubUserData } from '../actions/questionFour';

import debounce from '../utilities/debounce';

class QuestionFour extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tableHeaderData: ['Avatar', 'Login', 'Type', 'Score'],
    };
    this.currentValue = '';
    this.githubSearch = this.githubSearch.bind(this);
    this.debouncedGitHubSearch = debounce(this.githubSearch, 500);
  }

  /**
   *  Once the user has entered in three letters, we want to perform a lookup
   *  of users based on the value they have entered. Also clear the store data
   *  if they delete everything. We make sure the critical search call is
   *  debounced so that we aren't making crazy amounts of calls.
   *
   *  @param {object} e - React synthetic event triggered by out input change.
   */
  onInputChange(e) {
    debounce(this.aFunction, 1500);

    const value = e.target.value;
    this.currentValue = value;

    if (value.length > 2) {
      this.debouncedGitHubSearch();
    } else if (value.length === 0 && !!this.props.githubUserData.length) {
      this.props.setGithubUserData([]);
    }
  }

  /**
   *  Perform a search of github users with the current value we have set
   *  from the input change. Make sure the value is still higher than one,
   *  or we run into issues with the debounced value being a bit older.
   */
  githubSearch() {
    if (this.currentValue.length > 2) {
      this.props.searchGithubUsers(this.currentValue)
      .catch((error) => {
        console.log('An error occured on the github lookup:');
        console.log(error);
      });
    }
  }

  render() {
    return (
      <div>
        <h3>Github live user search!</h3>
        <p>
          Please enter a name:
          {' '}
          <input
            type="text"
            name="name"
            onChange={e => this.onInputChange(e)}
          />
          {' '}
          {this.props.isLoading &&
            <Loading />
          }
        </p>

        {this.props.githubUserData.length > 0 &&
          <Table
            data={this.props.githubUserData}
            headerData={this.state.tableHeaderData}
          />
        }
      </div>
    );
  }
}

QuestionFour.propTypes = {
  setGithubUserData: func.isRequired,
  searchGithubUsers: func.isRequired,
  isLoading: bool,
  githubUserData: arrayOf(shape({
    avatar_url: PropTypes.string,
    login: PropTypes.string,
    type: PropTypes.string,
    score: number,
  })),
};

QuestionFour.defaultProps = {
  isLoading: false,
  githubUserData: [{}],
};

function mapStateToProps(state) {
  return {
    githubUserData: state.githubUserData,
    isLoading: state.isLoading,
  };
}

function mapDispatchToProps(dispatch) {
  const actionCreators = {
    setGithubUserData,
    searchGithubUsers,
  };

  return bindActionCreators(actionCreators, dispatch);
}

const QuestionFourContainer = connect(mapStateToProps, mapDispatchToProps)(QuestionFour);

export default QuestionFourContainer;
