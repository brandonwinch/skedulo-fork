import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { bandData, currentTime } from './questionBonus';
import { peopleData, interestData, skillData, richest } from './questionThree';
import { githubUserData, isLoading } from './questionFour';

const rootReducer = combineReducers({
  bandData,
  currentTime,
  peopleData,
  interestData,
  skillData,
  richest,
  githubUserData,
  isLoading,
  routing: routerReducer,
});

export default rootReducer;
