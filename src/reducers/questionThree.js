import { SET_RICHEST, SET_PEOPLE, SET_INTERESTS, SET_SKILLS } from '../constants';

function peopleData(state = [], action) {
  switch (action.type) {
    case SET_PEOPLE :
      return [...action.people];
    default:
      return state;
  }
}

function interestData(state = [], action) {
  switch (action.type) {
    case SET_INTERESTS :
      return [...action.interests];
    default:
      return state;
  }
}

function skillData(state = [], action) {
  switch (action.type) {
    case SET_SKILLS :
      return [...action.skills];
    default:
      return state;
  }
}

function richest(state = 0, action) {
  switch (action.type) {
    case SET_RICHEST :
      return action.richest.richestPerson;
    default:
      return state;
  }
}

export { peopleData, interestData, skillData, richest };
