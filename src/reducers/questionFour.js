import { SET_GITHUB_USER_DATA, SET_LOADING } from '../constants';

function githubUserData(state = [], action) {
  switch (action.type) {
    case SET_GITHUB_USER_DATA :
      return [...action.userData];
    default:
      return state;
  }
}

function isLoading(state = false, action) {
  switch (action.type) {
    case SET_LOADING :
      return action.loadingState;
    default:
      return state;
  }
}

export { isLoading, githubUserData };
