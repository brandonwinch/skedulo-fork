// Setting default state on these reducers which never changes.

function bandData(state = []) {
  return state;
}

function currentTime(state = {}) {
  return state;
}

export { bandData, currentTime };
