import * as constants from '../constants';

/**
 *  Set the collective list of skills that people have.
 *
 *  @param {array} skills - Skills of people with personIds included for reference.
 *
 *  @return {object} - Updated collection or skills for our reducer to take in.
 */
export function setSkills(skills) {
  return ({
    type: constants.SET_SKILLS,
    skills,
  });
}

/**
 *  Set the collective list of interests that people have.
 *
 *  @param {array} interests - Interests of people with personIds included for reference.
 *
 *  @return {object} - Updated collection of interests for our reducer to take in.
 */
export function setInterests(interests) {
  return ({
    type: constants.SET_INTERESTS,
    interests,
  });
}

/**
 *  Set the collective list of people we have fetched.
 *
 *  @param {array} people - Collection of people with basic information.
 *
 *  @return {object} - Updated collection of people for our reducer to take in.
 */
export function setPeople(people) {
  return ({
    type: constants.SET_PEOPLE,
    people,
  });
}

/**
 *  Set the collective list of interests that people have.
 *
 *  @param {array} richest - Collection of which people are the richest.
 *
 *  @return {object} - Updated collection of richest data for our reducer to take in.
 */
export function setRichest(richest) {
  return ({
    type: constants.SET_RICHEST,
    richest,
  });
}

/**
 *  Fetch a list of interests based on the person ids passed in.
 *
 *  @param {string} ids - List of ids we want to look up: EG: '1,2,3,4'
 *
 *  @return {promise} - Resolving with correct interests data and setInterests, otherwise error.
 */
const fetchInterests = ids => (
  new Promise((resolve, reject) => {
    fetch(`/api/interests?personIds=${ids}`)
    .then(response => response.json())
    .then((data) => {
      resolve({ response: data, func: setInterests });
    })
    .catch((error) => {
      reject(error);
    });
  })
);

/**
 *  Fetch a list of skills based on the person ids passed in.
 *
 *  @param {string} ids - List of ids we want to look up: EG: '1,2,3,4'
 *
 *  @return {promise} - Resolving with correct skills data and setSkills, otherwise error.
 */
const fetchSkills = ids => (
  new Promise((resolve, reject) => {
    fetch(`/api/skills?personIds=${ids}`)
    .then(response => response.json())
    .then((data) => {
      resolve({ response: data, func: setSkills });
    })
    .catch((error) => {
      reject(error);
    });
  })
);

/**
 *  Fetch who the richest person is.
 *
 *  @return {promise} - Resolve with who the richest person is and setRichest function, or error.
 */
const fetchRichest = () => (
  new Promise((resolve, reject) => {
    fetch('/api/richest')
    .then(response => response.json())
    .then((data) => {
      resolve({ response: data, func: setRichest });
    })
    .catch((error) => {
      reject(error);
    });
  })
);

/**
 *  Fetch all of the people for this exercise. If successful, dispatch an action
 *  that updates our store with the updated people data.
 *
 *  @return {promise} - Resolve with ids so that we can look up other details in the future. || err
 */
export const fetchPeople = () => dispatch => (
  new Promise((resolve, reject) => {
    fetch('/api/people')
    .then(response => response.json())
    .then((data) => {
      const ids = data.map(person => person.id).join(',');
      dispatch(setPeople(data));
      resolve(ids);
    })
    .catch((error) => {
      reject(error);
    });
  })
);

/**
 *  Fetch interests, skills and richest data by using a promise all. After we have all of
 *  this data we can then proceed to update everything by calling our action creators.
 *
 *  @param {string} ids - Ids of people we wnt to look up. IE: '1,2,3,4'
 *
 *  @return {promise} - Execute all the promises listed in the Promise.all.
 */
export const fetchInterestsSkillsRichest = ids => dispatch => (
  Promise.all([fetchSkills(ids), fetchInterests(ids), fetchRichest()]).then((results) => {
    results.forEach((result) => {
      dispatch(result.func(result.response));
    });
  })
);
