import * as constants from '../constants';

/**
 *  Set github user data after a query to the API call has been made.
 *
 *  @param {array} - Our collection of user data.
 *
 *  @return {object} - Updated github user data.
 */
export function setGithubUserData(userData) {
  return ({
    type: constants.SET_GITHUB_USER_DATA,
    userData,
  });
}

/**
 *  Update our loading state.
 *
 *  @param {bool} loadingState - Whether or not we're currently loading or not.
 *
 *  @return {object} - Updated loading state.
 */
export function setLoadingState(loadingState) {
  return ({
    type: constants.SET_LOADING,
    loadingState,
  });
}

/**
 *  Make an api call to github and retrieve a list of users based on the query
 *  we have obtained from the input field. Once done, update the store.
 *
 *  @param {string} user - The current users we want to look up.
 *
 *  @return {promise} - Successful api call made or not.
 */
export const searchGithubUsers = user => dispatch => (
  new Promise((resolve, reject) => {
    // we don't get any content length response headers from this call,
    // so will need to be just a default 'loading' term.
    dispatch(setLoadingState(true));

    fetch(`https://api.github.com/search/users?per_page=100&q=${user}`)
    .then(response => response.json())
    .then((data) => {
      // we only want specific items from this call, so we'll map our response
      // to what we need.
      const mappedItems = data.items.map((val) => {
        const gitHubUser = {
          img_url: val.avatar_url,
          login: val.login,
          type: val.type,
          score: val.score,
        };

        return gitHubUser;
      });

      dispatch(setGithubUserData(mappedItems));

      resolve();
      dispatch(setLoadingState(false));
    })
    .catch((error) => {
      reject(error);
      dispatch(setLoadingState(false));
    });
  })
);
