import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { syncHistoryWithStore } from 'react-router-redux';
import { createBrowserHistory } from 'history';
import rootReducer from './reducers';
import { bandData, currentTime } from './data/bonusData';

// can set a default state for this since no fetching needs to be done.
const defaultState = {
  bandData,
  currentTime,
};

// see the store in redux extension
const enhancers = compose(
  applyMiddleware(thunk),
  window.devToolsExtension ? window.devToolsExtension() : f => f,
);

const store = createStore(
  rootReducer,
  defaultState,
  enhancers,
);

export const history = syncHistoryWithStore(createBrowserHistory(), store);

export default store;
