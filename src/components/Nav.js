import React from 'react';
import { Link } from 'react-router-dom';

// Basic nav component.
const Nav = () => (
  <ul>
    <li>
      <Link to="/q-one">Question One</Link>
    </li>
    <li>
      <Link to="/q-two">Question Two</Link>
    </li>
    <li>
      <Link to="/q-three">Question Three</Link>
    </li>
    <li>
      <Link to="/q-four">Question Four</Link>
    </li>
    <li>
      <Link to="/bonus">Bonus Question</Link>
    </li>
  </ul>
);

export default Nav;
