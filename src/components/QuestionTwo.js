import React from 'react';

const Component = () => {
  /**
   *  Given an array of order products coming in, we want to populate each items name and product price.
   *  We do this by querying the Products table with the IDs of our ordered products, 
   *
   *  @param {array} orderProducts - Array of items that need to have 'name' and 'price' populated.
   *
   *  @return {array} updatedOrderedProducts - Our order products now filled with name and price values.
   */
  const populateOrderedProducts = (orderProducts) => {
    const updatedOrderedProducts = [...orderProducts];
    const idsToFetch = updatedOrderedProducts.map(updatedOrderedProduct => (
      updatedOrderedProduct.ProductId
    ));
    const sqlStatement = // I haven't written SQL before, but should be something that selects the products with the IDs we pass in as an argument.

    // use our sqlStatement and pass in the an array of IDs for it to look up and then return.
    fetchProducts(sqlStatement, idsToFetch, (products) => {
      if (products.length > 0) {
        products.forEach((product) => {
          const productName = product.Name;
          const productPrice = product.Price;
          const productId = product.ProductId;

          // find where our current product lives in our orderedProduct array
          const indexOfProduct = updatedOrderedProducts.findIndex((orderedProduct) => (
            orderedProduct.ProductId === productId
          ));

          // update the ordered product details.
          if (indexOfProduct !== -1) {
            updatedOrderedProducts[indexOfProduct].Name = productName;
            updatedOrderedProducts[indexOfProduct].Price = productPrice;
          }
        });
      } else {
        // no products found.
      }
    });

    // return the updated orderedProducts
    return updatedOrderedProducts;
  };

  return (
    <div>
      I have written a pseudocode answer for this that does not compile or run.
      Please look at source to see answer.
    </div>
  )
};

export default Component;