import React from 'react';
import Nav from './Nav';

const Home = () => (
  <div>
    <Nav />
    Welcome to the home page. Please click above to view the answers to the questions.
  </div>
);

export default Home;
