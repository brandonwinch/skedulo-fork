import React from 'react';
import PropTypes, { arrayOf, shape } from 'prop-types';

/**
 *  A reusable table that can generate rows and cells depending on what data is
 *  passed into it.
 *
 *  Every object in 'data' will be looped through onto cells on its own row.
 *  The className key is reserved for any special classes you want to attach
 *  to this row.
 *
 *  headerData works the same way, but with only one row.
 *
 *  IE:
 *  data = [
 *    { // row1
 *      className: 'bold',
 *      cell1: 'value1',
 *      cell2: 'value2',
 *    },
 *    { // row2
 *      cell1: 'value1',
 *      cell2: 'value2',
 *    },
 *  ]
 *
 *  @param {object} props - Properties we want to populate the table with.
 */

class Table extends React.Component {

  // Only need to compare props here, as we have no react state.
  shouldComponentUpdate(nextProps) {
    if (JSON.stringify(this.props) === JSON.stringify(nextProps)) {
      return false;
    }

    return true;
  }

  /**
   *  Return the content that we want to see inside the TD cells.
   *
   *  @param {string} key - What key the td cell has.
   *  @param {string} value - The value of this key/value pair.
   *
   *  @return {string} - React component as a string to render out.
   */
  generateTdContent(key, value) {
    if (key === 'img_url') {
      return (
        <img
          src={value}
          alt="avatar"
        />
      );
    }

    return value;
  }

  /**
   *  By mapping over the keys of rowData, we can find out how many
   *  TD elements we want to render. This will do that and return all of
   *  the cells for a particular row.
   *
   *  @param {object} rowData - An object that has all of the info we want for this row.
   *  @param {number} rowNumber - Used so we can put a proper key on the cell.
   *
   *  @return {string} - React cell components to render.
   */
  generateTdElements(rowData, rowNumber) {
    const tableCells = Object.keys(rowData).map((key, idx) => {
      const cellKey = `${rowNumber}-cell-${idx}`;

      if (key !== 'className') {
        const tdContent = this.generateTdContent(key, rowData[key]);

        return (
          <td
            key={cellKey}
            className={rowData.className}
          >
            {tdContent}
          </td>
        );
      }
    });

    return tableCells;
  }

  /**
   *  Generate all of the rows we want from the table by mapping through
   *  all of our objects in data and returning the rows as a react string
   *  to render.
   *
   *  @return {string} - React rows to render in a tbody.
   */
  generateRows() {
    return this.props.data.map((rowData, idx) => {
      const rowKey = `row-${idx}`;
      const tdElements = this.generateTdElements(rowData, idx);

      return (
        <tr key={rowKey}>
          {tdElements}
        </tr>
      );
    });
  }

  /**
   *  Generate the 'header' row. We iterate through header data and return
   *  whatever value is in there for that TD cell.
   *
   *  @return {string} - A single row to render in thead.
   */
  generateHeader() {
    return this.props.headerData.map((headerValue, idx) => {
      const headerKey = `header-${headerValue}-${idx}`;

      return (
        <td key={headerKey}>
          {headerValue}
        </td>
      );
    });
  }

  render() {
    // headers may not exist, so define it empty first.
    let header = [];
    if (this.props.headerData) {
      header = this.generateHeader();
    }

    const rows = this.generateRows();

    return (
      <table>
        <thead>
          <tr>
            {header}
          </tr>
        </thead>
        <tbody>
          {rows}
        </tbody>
      </table>
    );
  }
}

Table.propTypes = {
  headerData: arrayOf(PropTypes.string),
  data: arrayOf(shape({})).isRequired,
};

Table.defaultProps = {
  headerData: [],
};

export default Table;
