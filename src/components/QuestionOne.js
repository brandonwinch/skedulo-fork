import React from 'react';

const QuestionOne = () => {
  /**
   *  Map through an array of items and find out the value should be. Return them
   *  as something we can render.
   */
  const content = [...new Array(100)].map((_, i) => {
    const key = `count-${i}`;
    const num = i + 1;
    const value = (num % 4 ? '' : 'Hello') + (num % 5 ? '' : 'World') || num.toString();

    return (
      <li key={key}>
        {value}
      </li>
    );
  });

  return (
    <ul>
      {content}
    </ul>
  );
};

export default QuestionOne;
