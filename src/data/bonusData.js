function addMin(date, mins) {
  return new Date(date.getTime() + (mins * 60000));
}

const currentTime = new Date();

/**
  Timetable:
   ---------------------------------------------------
  | 1m   |        |        | BAND 3 |        |        |
  | 2m   | BAND 1 |        | BAND 3 |        |        |
  | 3m   |        | BAND 2 | BAND 3 |        |        |
  | 4m   |        | BAND 2 |        |        |        |
  | 5m   |        |        |        |        |        |
  | 6m   |        |        |        | BAND 4 | BAND 5 |
   ---------------------------------------------------

Output should be something like this:

1st minute: BAND 3
2nd minute: BAND 1
3rd minute: BAND 3
4th minute: BAND 2
5th minute: (wait)
6th minute: (random)
**/

const bandData = [
  {
    band: 'Band 1',
    start: addMin(currentTime, 1),
    finish: addMin(currentTime, 2),
    priority: 1,
  },
  {
    band: 'Band 2',
    start: addMin(currentTime, 2),
    finish: addMin(currentTime, 4),
    priority: 3,
  },
  {
    band: 'Band 3',
    start: currentTime,
    finish: addMin(currentTime, 3),
    priority: 2,
  },
  {
    band: 'Band 4',
    start: addMin(currentTime, 5),
    finish: addMin(currentTime, 6),
    priority: 4,
  },
  {
    band: 'Band 5',
    start: addMin(currentTime, 5),
    finish: addMin(currentTime, 6),
    priority: 4,
  },
];

export { bandData, currentTime };
