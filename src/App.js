import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { HashRouter, Switch, Route } from 'react-router-dom';
import store, { history } from './store';

import QuestionOne from './components/QuestionOne';
import QuestionTwo from './components/QuestionTwo';
import { QuestionThreeContainer } from './containers/QuestionThree';
import QuestionFourContainer from './containers/QuestionFour';
import { QuestionBonusContainer } from './containers/QuestionBonus';
import Home from './components/Home';

const App = () => (
  <Provider store={store}>
    <HashRouter history={history}>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/q-one" component={QuestionOne} />
        <Route path="/q-two" component={QuestionTwo} />
        <Route path="/q-three" component={QuestionThreeContainer} />
        <Route path="/q-four" component={QuestionFourContainer} />
        <Route path="/bonus" component={QuestionBonusContainer} />
      </Switch>
    </HashRouter>
  </Provider>
);

render(<App />, document.getElementById('root'));
